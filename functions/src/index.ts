import * as functions from 'firebase-functions';
import * as cors from 'cors';

const admin = require('firebase-admin');

const serviceAccount = require('../doubt-s-firebase-adminsdk.json');

const corsHandler = cors({ origin: true });
const universal = require(`../dist/app/server/main`).app();

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://doubt-s.firebaseio.com'
});
const firestore = admin.firestore();
// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
export const getQuestionsByQid = functions.https.onRequest((request: any, response: any) => corsHandler(request, response, () => {
    const body = request.body;

    try {
        firestore.collection('questions').where('qid', '==', body.qid).get().then((snapshot: any) => {
            let data = {};
            snapshot.forEach((document: any) => {
                data = document.data();
            });
            response.status(200);
            response.json({ status: true, data });

        }).catch((e: any) => {
            response.status(200);
            response.json({ status: false, data: e });
        });
    } catch (e) {
        response.status(200);
        response.json({ status: false, data: e });
    }
}));

export const createNewRating = functions.https.onRequest((request: any, response: any) => corsHandler(request, response, () => {
    const body = request.body;

    try {
        firestore.collection('rating').add(body).then((resp:any) => {
            response.status(200);
            response.json({ status: true, data  : resp});
        }).catch((e: any) => {
            response.status(200);
            response.json({ status: false, data: e });
        });
    } catch (e) {
        response.status(200);
        response.json({ status: false, data: e });
    }
}));

export const createNewRequest = functions.https.onRequest((request: any, response: any) => corsHandler(request, response, () => {
    const body = request.body;

    try {
        firestore.collection('requests').add(body).then((resp:any) => {
            response.status(200);
            response.json({ status: true, data  : resp});
        }).catch((e: any) => {
            response.status(200);
            response.json({ status: false, data: e });
        });
    } catch (e) {
        response.status(200);
        response.json({ status: false, data: e });
    }
}));




export const ssr = functions.https.onRequest(universal);
