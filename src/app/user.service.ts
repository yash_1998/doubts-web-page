import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  subjectName = '';
  classData = {} as any;
  subjectData = {} as any;
  chaperData = {} as any;
  subjectsmenu = []; classesmenu = []; unitsmenu = []; chaptersmenu = [];

  myorders_subject = ''; myorders_class = ''; myorders_chapter = '';
  clicked_subject_code = ''; clicked_class_code: any = ''; clicked_unit_code = ''; clicked_chapter_code = '';
  clicked_concept_code: any = '';
  search_q_docid = ''; search_q_conceptcode = ''; loggedInUserNo = ""; loggedInUserName = "";

  workdesc; assignto; follower; docId; registered_username; public appmenu: any[];
  viewlead_popup1_clickedid; viewlead_popup1_clicked_email;
  crudcalltype = ""; stopbell = 'Y';

  viewcomplaint_clicked_ticketid;
  viewcomplaint_clicked_complaint_agent_name; viewcomplaint_clicked_complaint_status;
  viewcomplaint_clicked_complaint_item_serial_no; viewcomplaint_clicked_complaint_priority;
  viewcomplaint_clicked_complaint_purchase_date_show; viewcomplaint_clicked_complaint_under_warranty;
  viewcomplaint_clicked_complaint_addess;
  viewcomplaint_clicked_id; viewcomplaint_clicked_alternateno;

  vieworders_clicked_desc; viewcomplaint_clicked_fileURL = ''; viewcomplaint_clicked_filename = '';

  preview_ques_qid = '';

  quesarray; quesarray2; unitsarray2; selected_ques_chapter;
  //project tabs variable
  selected_project_to_view; video_url = '';
  loading: HTMLIonLoadingElement;
  constructor(public _http: HttpClient, public loadingController: LoadingController) { }

}
