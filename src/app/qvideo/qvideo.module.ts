import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QvideoPageRoutingModule } from './qvideo-routing.module';

import { QvideoPage } from './qvideo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QvideoPageRoutingModule
  ],
  declarations: [QvideoPage]
})
export class QvideoPageModule {}
