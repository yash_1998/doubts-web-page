import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QvideoPage } from './qvideo.page';

const routes: Routes = [
  {
    path: '',
    component: QvideoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QvideoPageRoutingModule {}
