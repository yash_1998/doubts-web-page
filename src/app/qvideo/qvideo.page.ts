import { ChangeDetectorRef, Component, OnInit, SecurityContext } from '@angular/core';
import { UserService } from '../user.service';



declare var require: any;
import { File } from '@ionic-native/file/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';

import { NavController } from '@ionic/angular';
import { CrudService } from './../service/crud.service';


//http

import 'rxjs/add/operator/map';

//notification toast
import { ToastController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-qvideo',
  templateUrl: './qvideo.page.html',
  styleUrls: ['./qvideo.page.scss'],
})
export class QvideoPage implements OnInit {
video_url;safeUrl:any;
qid='';

  
  paramQid = '';
  val_phno='';val_rule=''; today_date='';msg='';
  view_desc=''; view_sol='';  goalList: any[] = [];segment='';pdf_url='';
  constructor(public cdr : ChangeDetectorRef,public activatedRoute :ActivatedRoute,public domSanitizer: DomSanitizer,public user:UserService) { 
    this.domSanitizer = domSanitizer;
  }

  getTrustedUrl(url:any){ 
    this.safeUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
   }
  
  ngOnInit() {
  
  if(this.user && this.user.video_url){
    console.log(this.user.video_url);
    var link = this.user.video_url;
    var arr = link.split("/");
    console.log(arr);
    
    this.video_url = "https://www.youtube.com/embed/"+arr[3];
    this.domSanitizer.sanitize(SecurityContext.URL, this.video_url);
    console.log(this.video_url, "++");
    this.getTrustedUrl(this.video_url);
  }
   
    // var doc  = document.getElementById("v_ideo");
    // (<any>doc).src = this.domSanitizer.bypassSecurityTrustResourceUrl(this.video_url);
  }

 

}
