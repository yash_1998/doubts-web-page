import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

import { NgxMetaTagsService } from 'ngx-meta-tags';


declare let require: any;

import { File } from '@ionic-native/file/ngx';

import { NavController } from '@ionic/angular';
import { CrudService } from './../service/crud.service';


//http
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import {Location} from '@angular/common'; 
//notification toast
import { ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';



//ionic cordova plugin add cordova-plugin-file-transfer
//npm install @ionic-native/file-transfer
//npm i @ionic-native/file
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  paramQid = '';
  val_phno = ''; val_rule = ''; today_date = ''; msg = '';
  qid = ''; view_desc = ''; view_sol = ''; goalList: any[] = []; segment = ''; pdf_url = '';
  constructor(
    private ngxMetaTagsService: NgxMetaTagsService,
    private readonly meta: Meta,
    public router : Router,
    public titleService : Title,
    private file: File,
    private transfer: FileTransfer,
    public location :Location,
    public cdr: ChangeDetectorRef, public activatedRoute: ActivatedRoute, private http: HttpClient, public toastController: ToastController, private crudService: CrudService, public user: UserService, public navCtrl: NavController) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(paramsId => {
console.log(this.router.url , window.location.href);
      this.paramQid = paramsId.quid;

      this.segment = '';
      this.view_desc = ''; this.view_sol = '';

      // this.firbaseAuth.signInAnonymously().then(resp => {
      this.loadData();
      // }).catch(error => {
      //   this.loadData();
      // });


      ;



      if (this.user.search_q_docid) {
        this.qid = this.user.search_q_docid;
      }

      console.log(this.qid, '28462834798349');

      console.log(this.qid);
    });
    const dt = new Date();
    const dd = dt.getDate();
    const mon = dt.getMonth();
    const yy = dt.getFullYear();
    const months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
    let dt1 = '';
    if (dd < 10) {
      dt1 = '0' + dd;
    }
    else {
      dt1 = dd.toString();
    }
    const date = dt1 + '-' + months[mon] + '-' + yy;
    console.log(date);
    this.today_date = date;

  }

  loadData() {
    this.crudService.getQuestionsById({ qid: this.paramQid }).subscribe(resp => {

      if (resp.status) {
        const questionData = resp.data;
        console.log(questionData , 'question data');
        this.view_desc = questionData.desc;
        this.view_sol = questionData.sol;
         if (this.paramQid && questionData.url) {
          //this.router.navigateByUrl(`/${this.paramQid}/${questionData.url}`);
          this.router.navigateByUrl(`/${questionData.url}/${this.paramQid}`);
        }
        // window.history.replaceState({}, '',`/${this.qid}/${questionData.url}`);
        this.ngxMetaTagsService.setMetaFromConfig({ title: 'test_title', description: questionData.meta_title });
        this.ngxMetaTagsService.setMetaFromConfig({ title: 'description', description: questionData.meta_desc });
        this.titleService.setTitle(questionData.meta_title);
        // this.meta.setTag('title', questionData.meta_title);
        // this.meta.setTag('description', questionData.meta_desc);
        this.meta.updateTag({ name: 'meta-title', content: questionData.meta_title });
        // this.meta.updateTag({ name: 'description', content: questionData.meta_desc });

        this.pdf_url = questionData.q_pdf_url;
        this.user.video_url = questionData.qvideo;

        this.user.search_q_conceptcode = questionData.conceptcode;
      }


    });


    // }
  }



  gohome() {
    this.navCtrl.navigateRoot('home2');
  }




  segmentChanged(ev){
    console.log(ev.detail.value);
    if(ev.detail.value == 'discover')
    {
      this.openpdf();
    }
    if(ev.detail.value == 'snapshot')
    {
      this.navCtrl.navigateForward(['/qapreview/q/qvideo']);
      // window.open('https://app.studyrocks.in/qvideo')
    }
    if(ev.detail.value == 'more'){
      window.open('https://app.studyrocks.in/concept')
     // this.navCtrl.navigateForward(['/concept']);
    }
    if(ev.detail.value == 'search'){
      window.open('https://app.studyrocks.in/trysimilar')
      //this.navCtrl.navigateForward(['/trysimilar']);
    }
    if(ev.detail.value == 'collection'){
      // window.open('https://app.studyrocks.in/rating')
     this.navCtrl.navigateForward(['/qapreview/q/rating']);
    }
  } 



  openpdf(){
    window.open(this.pdf_url, '_blank', 'fullscreen=yes');
  }
  downloadpdf(){
    const fileTransfer: FileTransferObject = this.transfer.create();
    const url = this.pdf_url;
  fileTransfer.download(url, this.file.dataDirectory + 'file.pdf').then((entry) => {
    console.log('download complete: ' + entry.toURL());
  }, (error) => {
    // handle error
  });
  }

  request_help(){
    if(!this.crudService.phone_no){
      this.crudService.showPrompt().then(resp => {
        console.log(resp, 'resp')
        resp.onDidDismiss().then(data => {
          console.log(data,'data')
          this.setData();
        })
      })
    }else {
this.setData();
    }
    
     
  }

  setData(){
    let record = {};
    record['user_phno'] = this.val_phno;
    record['status'] = 'OPEN';
  //  record['entryDate'] = firebase.firestore.FieldValue.serverTimestamp();
    record['entryDate'] = this.today_date;
    this.crudService.create_NewRequest(record).subscribe(resp => {
      console.log(resp);
      console.log(resp.id)
      if(resp && resp.status){
        //this.navCtrl.navigateForward('reqlive');
        this.notificationToast();
        this.sendtelegram();
      }
    })
  }

  async notificationToast() {
    const toast = await this.toastController.create({
      header: 'STUDYROCKS LIVE Person Help Center',
      message: "Request for LIVE session sent. Our Experts will contact you soon.",
      cssClass: "toast-scheme ",
      color: 'warning',
      buttons:[        {
    //    side: 'start',
    //    icon: 'star',
        text: 'Hide',
        handler: () => {
          console.log('Favorite clicked');
        }
      }],
      position: 'top',
      duration: 4000
    });
    toast.present();
  }

  sendtelegram()
  {
    //150152224,
    this.sendtelegram_serv('You got a new LIVE Teacher Request.', '1463706697').subscribe((data : any)=>{
      console.log(data);
      if(data.ok == true)
      {
        this.msg = 'success';
      }
    });
    
  }

  sendtelegram_serv(msg,chatid)
  {
  // var url_bot = "513946365:AAHQOvdFDwBKSGwz8EW7EzUQK5YBLSuji1Y";   // sapphire bot
  var url_bot = "1647627417:AAEJ_r5VnmbwSXw2NV6VSqeXt7PAqbBEvec"; // office plus bot
    var full_url = "https://api.telegram.org/bot"+url_bot+"/sendmessage?chat_id="+chatid+'&text='+msg;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return this.http.post(full_url,{headers:headers})
      .map((response:Response) => response.json());
  }



}
