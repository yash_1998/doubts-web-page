import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TrysimilarPage } from './trysimilar.page';

const routes: Routes = [
  {
    path: '',
    component: TrysimilarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TrysimilarPageRoutingModule {}
