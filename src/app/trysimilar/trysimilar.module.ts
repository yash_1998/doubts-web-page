import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TrysimilarPageRoutingModule } from './trysimilar-routing.module';

import { TrysimilarPage } from './trysimilar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TrysimilarPageRoutingModule
  ],
  declarations: [TrysimilarPage]
})
export class TrysimilarPageModule {}
