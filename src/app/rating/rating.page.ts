import { Component, OnInit, Input, EventEmitter ,Output } from '@angular/core';

import { NavController } from '@ionic/angular';
import { CrudService } from './../service/crud.service';


//notification toast
import { ToastController } from '@ionic/angular';

enum COLORS{
  GREY = "#E0E0E0",
  GREEN = "#76FF03",
  YELLOW = "#FFCA28",
  RED="#DD2C00"
}


@Component({
  selector: 'app-rating',
  templateUrl: './rating.page.html',
  styleUrls: ['./rating.page.scss'],
})
export class RatingPage implements OnInit {
  @Input() rating: number ;
  @Output() ratingChange: EventEmitter<number> = new EventEmitter();

  constructor(public toastController: ToastController,private crudService: CrudService) { }
  val_phno='';
  ngOnInit() {
    // this.storage.get('loggedInUser').then((val_phno) => {
    //   console.log('Your no: ', val_phno);
    //   this.val_phno = val_phno;
    // });
    this.crudService.showPrompt()
  }

  rate(index: number) {
    if(this.crudService.phone_no){
    this.rating = index;
    console.log(this.rating);
    this.ratingChange.emit(this.rating);

    let record = {};
    record['rating'] = this.rating;
    record['phno'] = this.crudService.phone_no;
    this.crudService.create_NewRating(record).subscribe(resp => {
      // console.log(resp);
      // console.log(resp.id)
      if(resp && resp.status){
        this.notificationToast();
      }
    })

  }else {
    this.crudService.showPrompt();
  }
    //   .catch(error => {
    //     console.log(error);
    //   });
      // function used to change the value of our rating 
      // triggered when user, clicks a star to change the rating
   }
  
  
  
  getColor(index: number) {
    if (this.isAboveRating(index)){
      return COLORS.GREY;
    }
  
    switch (this.rating){
      case 1:
      case 2:
        return COLORS.RED;
        case 3:
          return COLORS.YELLOW;
          case 4:
            case 5:
              return COLORS.GREEN;
              default:
                return COLORS.GREY;
    }
      /* function to return the color of a star based on what
       index it is. All stars greater than the index are assigned
       a grey color , while those equal or less than the rating are
       assigned a color depending on the rating. Using the following criteria:
    
            1-2 stars: red
            3 stars  : yellow
            4-5 stars: green 
      */
    }
  
  isAboveRating(index: number):boolean {
    return index > this.rating;
    // returns whether or not the selected index is above ,the current rating
    // function is called from the getColor function.
  }

  async notificationToast() {
    const toast = await this.toastController.create({
      header: 'STUDYROCKS',
      message: "Thank you for your feedback.",
      cssClass: "toast-scheme ",
      color: 'warning',
      buttons:[        {
    //    side: 'start',
    //    icon: 'star',
        text: 'Hide',
        handler: () => {
          console.log('Favorite clicked');
        }
      }],
      position: 'top',
      duration: 4000
    });
    toast.present();
  }

}
